# The Signal Provisioning process

1. Get an Instance of SignalAccountManager and provide it with some ServiceUrls and a user agent string. This contains the name of your application.
2. From there on you have to request a new device UUID from the servers. Afterwards, you have to generate an URL in the form of: ``tsdevice:/?uuid=[requested UUID]&pub_key=[Base64 encoded PublicKey]``
3. Generate a QR code containing this URL and scan it with your iOS/Android device.
4. The application doesn't have to wait until the user confirms the registration. Instead, it can instantly call submitLinkRegistration() from the account manager. The method blocks until linking is done.
5. It returns an object containing the most important information of your Signal account.

## Notes

- websocket location: ``/v1/websocket/provisioning/?agent=$agent``
- no requests to server, just respond with ``200, OK``
- keepalive uri: ``/v1/keepalive/provisioning``
- requests from server to client (this instance):
1. ``/v1/address`` - ProvisioningProto with uuid
2. Device scans the URI
3. ``/v1/message`` - ProvisionEnvelopeProto & decode Message (ProvisioningCipher)


## Signal-Desktop
- new ProvisioningCipher, add listener to get response if publickey is available
- publickey is available through keypair which is generated if not existing
- identity is global, therefore static
- if websocket gets closed before uri scan -> failure
- do procedure described above and decrypt provisioningMessage using the previously defined identity
- Gathered Information:
``
phoneNumber, provisioningCode, identityKeyPair, deviceName, userAgent
``
- generate `` signalingKey, password, registrationID ``
- PUT json with following information to server on `` /v1/devices/[provisioningCode] `` and authorize:
  ```json
  {
    "signalingKey": "[Base64 encoded signalingKey]",
    "supportsSms": false,
    "fetchesMessage": true,
    "registratitonId": "[registrationID]",
    "name": "[deviceName]"
  }  
  ```
- save all values into storage, deviceId is parsed from response:
  ```json
  {
    "deviceId": "[id]"
  }
  ```
- generateKeys and push them to server; done!