/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal.test;

import static org.junit.Assert.*;

import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import de.devpi.signal.*;

import org.junit.Test;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.internal.configuration.*;

public class SignalProvisionTest
{
	
	String url = "https://textsecure-service.whispersystems.org";
	String cdn = "https://cdn.signal.org";
	Path resource;
	TrustStore store;
	SignalServiceConfiguration serviceConfiguration;
	private SignalRegistration accountManager;
	
	public SignalProvisionTest()
			throws NoSuchAlgorithmException, URISyntaxException
	{
		resource = Paths.get(ClassLoader.getSystemResource("whisper.store").toURI());
		store = new TrustStoreImpl(resource, "whisper");
		serviceConfiguration = new SignalServiceConfiguration(
				new SignalServiceUrl[]{new SignalServiceUrl(url, store)},
				new SignalCdnUrl[]{new SignalCdnUrl(cdn, store)});
	}
	
	@Test
	public void getUUID()
	{
		accountManager = new SignalRegistration(serviceConfiguration);
		String uuid = null;
		try
		{
			uuid = accountManager.getNewDeviceUuid();
		}
		catch (IOException | TimeoutException e)
		{
			e.printStackTrace();
		}
		assertNotNull("uuid should not be null", uuid);
		//System.out.printf("The requested uuid is %s\n", uuid);
	}
	
	@Test
	public void idCreation()
	{
		accountManager = new SignalRegistration(serviceConfiguration);
		RenderedImage qr = accountManager.getAuthenticationQRCode();
		assertNotNull("Image should not be null, Exception ocurred", qr);
	}
	
}
