/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix

import java.nio.charset.StandardCharsets.*
import java.security.MessageDigest
import java.util.*

/**
 * Returns the fully qualified name of the calling class. If this class is of type `KotlinfilenameKt`, we replace it with
 * `kotlinfilename.kt`. This is helpful to create a logger for kotlin files that do
 * not contain a class or an object.
 */
fun currClassName() : String
{
	val stackTrace = Thread.currentThread().stackTrace
	//println(stackTrace.joinToString(",") { it.className })
	// we assume that the first is Thread, the second is the currClass call, so lets take a look at the third
	var name = stackTrace[2].className
	val regex = Regex("\\.([^\\.]*)Kt$")
	name = name.replace(regex, { "/" + it.groupValues[1][0].toLowerCase() + it.groupValues[1].substring(1) + ".kt" })
	return name
}

private val sha1md = MessageDigest.getInstance("SHA-1")
private val base64enc = Base64.getEncoder()

fun sha1(bytes : ByteArray) = base64enc.encodeToString(sha1md.digest(bytes))
fun sha1(string : String) = sha1(string.toByteArray(UTF_8))

