/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.db

import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.ReferenceOption.*
import org.whispersystems.libsignal.*
import org.whispersystems.libsignal.state.*

object SignalStorages : LongIdTable(name = "signal_storage")
{
	val bridgeUser = reference("bridge_user", BridgeUsers, CASCADE)
	val registrationId = integer("registration_id")
	val encrypted = bool("encrypted").default(false)
	val signalUser = text("signal_user")
	val signalPassword = text("signal_pw")
	val signalingKey = text("signalingKey")
	val identityKeyPair = text("identity_key_pair")
	val nextPreKey = integer("next_pre_key").default(0)
	val nextSignedPreKey = integer("next_signed_pre_key").default(0)
	val salt = text("salt").default(getRString(8))
	val iv = text("iv").default("")
}

object SignalIdentityKeys : LongIdTable(name = "signal_identity_key")
{
	val storage = reference("storage", SignalStorages, CASCADE)
	val name = text("name")
	val deviceId = integer("device_id")
	val key = text("key")
}

class SignalIdentityKey(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<SignalIdentityKey>(SignalIdentityKeys)
	
	var storage by SignalStorage referencedOn SignalIdentityKeys.storage
	
	private var name by SignalIdentityKeys.name
	private var deviceId by SignalIdentityKeys.deviceId
	var address : SignalProtocolAddress
		get() = SignalProtocolAddress(name, deviceId)
		set(value)
		{
			name = value.name; deviceId = value.deviceId
		}
	
	private var key0 by SignalIdentityKeys.key
	var key : IdentityKey
		get() = IdentityKey(storage.decryptValue(key0), 0) // SQL starts with index 1
		set(value)
		{
			key0 = storage.encryptValue(value.serialize())
		}
}

object SignalPreKeys : LongIdTable(name = "signal_pre_key_record")
{
	val storage = reference("storage", SignalStorages, CASCADE)
	val keyId = integer("key_id")
	val signed = bool("signed")
	val key = text("key")
}

class SignalPreKey(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<SignalPreKey>(SignalPreKeys)
	
	var storage by SignalStorage referencedOn SignalPreKeys.storage
	
	var keyId by SignalPreKeys.keyId
	var signed by SignalPreKeys.signed
	
	private var key0 by SignalPreKeys.key
	var key : PreKeyRecord
		get() = PreKeyRecord(storage.decryptValue(key0))
		set(value)
		{
			key0 = storage.encryptValue(value.serialize())
		}
	var signedKey : SignedPreKeyRecord
		get() = SignedPreKeyRecord(storage.decryptValue(key0))
		set(value)
		{
			key0 = storage.encryptValue(value.serialize())
		}
}

object SignalSessions : LongIdTable(name = "signal_session")
{
	val storage = reference("storage", SignalStorages, CASCADE)
	val name = text("name")
	val deviceId = integer("device_id")
	val session = text("session")
}

class SignalSession(id : EntityID<Long>) : LongEntity(id)
{
	companion object : LongEntityClass<SignalSession>(SignalSessions)
	
	var storage by SignalStorage referencedOn SignalSessions.storage
	
	private var name by SignalSessions.name
	private var deviceId by SignalSessions.deviceId
	var address : SignalProtocolAddress
		get() = SignalProtocolAddress(name, deviceId)
		set(value)
		{
			name = value.name; deviceId = value.deviceId
		}
	
	private var session0 by SignalSessions.session
	var session : SessionRecord
		get() = SessionRecord(storage.decryptValue(session0))
		set(value)
		{
			session0 = storage.encryptValue(value.serialize())
		}
}
