/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

/*
package de.devpi.matrix.encryption

import com.beust.klaxon.*
import msrd0.matrix.client.event.EventEncryptor
import org.matrix.olm.*

val utility : OlmUtility = OlmUtility()

/**
 * Enrypt/Decrypt messages for a single room using Olm
 *
 *
 */
class RoomEncryptorOlm(var userID : String, var deviceID : String, var roomID : String) : EventEncryptor
{
	//TODO: implement serialization
	var account : OlmAccount = OlmAccount()
	var sessions : MutableMap<String, MutableList<KeyStore>> = mutableMapOf()
	var outMegolmSession : OlmOutboundGroupSession = OlmOutboundGroupSession()
	var inMegolmSession : OlmInboundGroupSession = OlmInboundGroupSession(outMegolmSession.sessionKey())
	private var hasSession : Boolean = false
	
	fun getMegolmSession(renew : Boolean = false) : JsonObject
	{
		//TODO: output as toDeviceJson
		if (hasSession && renew)
		{
			if (!outMegolmSession.isReleased)
				outMegolmSession.releaseSession()
			if (!inMegolmSession.isReleased)
				inMegolmSession.releaseSession()
			outMegolmSession = OlmOutboundGroupSession()
			val sessionKey : String = outMegolmSession.sessionKey()
			inMegolmSession = OlmInboundGroupSession(sessionKey)
		}
		
		val megolmJson = JsonObject()
		megolmJson["algorithm"] = "m.megolm.v1.aes-sha2"
		megolmJson["room_id"] = roomID
		megolmJson["session_id"] = outMegolmSession.sessionIdentifier()
		megolmJson["session_key"] = outMegolmSession.sessionKey()
		
		val encryptEvent = JsonObject()
		encryptEvent["type"] = "m.room_key"
		encryptEvent["content"] = megolmJson
		
		val result = JsonObject()
		
		for ((recipient, data) in sessions)
		{
			for ((deviceId, identityKey, olmSession) in data)
			{
				if (olmSession != null)
				{
					val olmJson = JsonObject()
					olmJson["algorithm"] = "m.olm.v1.curve25519-aes-sha2"
					olmJson["sender_key"] = getIdentityKey()
					
					val message : OlmMessage = olmSession.encryptMessage(encryptEvent.toJsonString(canonical = true))
					
					olmJson.mapNested("ciphertext.$identityKey.type", message.mType)
					olmJson.mapNested("ciphertext.$identityKey.body", message.mCipherText)
					
					result.mapNested("messages.$recipient.$deviceId", olmJson)
				}
			}
		}
		return result
	}
	
	/**
	 * Process events of type ```m.room.encrypted```
	 */
	override fun getDecryptedJson(event : JsonObject) : JsonObject
	{
		//TODO("where to get deviceid from?")
		if (event["type"] == "m.room.encrypted")
		{
			if (event.getNested("content.algorithm") == "m.olm.v1.curve25519-aes-sha2")
			{
				val otherDeviceKey = event.getNested("content.sender_key") as String
				val cipherMessage = event.getNested("content.ciphertext.$deviceID.body") as String
				val type = event.getNested("content.ciphertext.$deviceID.type") as Int
				val currentKeyStore = sessions[event["sender"]]
				
				if (currentKeyStore == null)
				{
				
				}
				
				
				if (type == 0)
				{
				
					
				}
				
				val olmMessage = OlmMessage()
				olmMessage.mCipherText = cipherMessage
				olmMessage.mType = type.toLong()
				val jsonString = thisOlmSession.decryptMessage(olmMessage)
				val decryptedJson = Parser().parse(jsonString) as JsonObject
				if (decryptedJson["type"] == "m.room_key")
				{
					val megolmJson = decryptedJson["content"] as JsonObject
					if (megolmJson["session_key"] != outMegolmSession.sessionKey())
					{
						inMegolmSession.releaseSession()
						outMegolmSession.releaseSession()
						inMegolmSession = OlmInboundGroupSession(megolmJson["session_key"] as String)
						outMegolmSession = OlmOutboundGroupSession()
						
					}
				}
			}
			
		}
		else
			return event
	}
	
	private fun changeOlmSessionFromMessage(message : String, deviceKey : String, userId : String)
	{
		val otherSession = OlmSession()
		otherSession.initInboundSessionFrom(account, deviceKey, message)
		val newKeyStore = KeyStore(deviceId, deviceKey, otherSession)
		sessions[userId] = newKeyStore
	}
	
	/**
	 * Process any events and return them as ```m.room.encrypted```
	 *
	 * Remember to add event metadata afterwards!
	 */
	override fun getEncryptedJson(event : JsonObject) : JsonObject
	{
		TODO()
	}
	
	/**
	 * Retrieve the JsonObject used to establish a new encrypted sessions with previous allocated devices.
	 *
	 * Remember to send this event using the ```sendToDevice``` API provided by Matrix. It is also important to set the
	 * event type to ```m.room.encrypted```
	 *
	 * @link addDeviceKeys(String,String,String,String ...)
	 */
	override fun exchangeKeys() : JsonObject
			= getMegolmSession(true)
	
	/**
	 *
	 */
	override fun addDeviceKeys(userId : String, deviceId : String, identityKey : String, vararg additionalKeys : String)
	{
		var keyStore = sessions[userId]
		if (keyStore == null)
		{
			if (additionalKeys.size == 1)
			{
				val olmSession : OlmSession = OlmSession()
				olmSession.initOutboundSession(account, identityKey, additionalKeys[0])
				keyStore = KeyStore(deviceId, identityKey, olmSession)
			}
			else
				keyStore = KeyStore(deviceId, identityKey, null)
			sessions[userId] = keyStore
		}
	}
	
	override fun uploadKeysJson() : JsonObject
	{
		val keyMap = account.identityKeys()
		val topJson = JsonObject()
		var jsonObj = JsonObject()
		jsonObj["user_id"] = userID
		jsonObj["algorithms"] = JsonArray("m.olm.v1.curve25519-aes-sha2", "m.megolm.v1.aes-sha2")
		jsonObj.mapNested("keys.curve25519:$deviceID", keyMap["curve25519"])
		jsonObj.mapNested("keys.ed25519:$deviceID", keyMap["ed25519"])
		jsonObj["device_id"] = deviceID
		topJson["device_keys"] = signJson(jsonObj)
		jsonObj.clear()
		account.generateOneTimeKeys(30)
		val oneTimeKeys : MutableMap<String, String>? = account.oneTimeKeys()["curve25519"]
		if (oneTimeKeys != null)
			for ((id, key) in oneTimeKeys)
			{
				jsonObj["key"] = key
				jsonObj = signJson(jsonObj)
				topJson.mapNested("one_time_keys.signed_curve25519:$id", jsonObj)
				jsonObj.clear()
			}
		return topJson
	}
	
	fun signJson(jsonObj : JsonObject) : JsonObject
	{
		val signature = account.signMessage(jsonObj.toJsonString(canonical = true))
		if (verifySignature(signature, getDeviceKey(), jsonObj.toJsonString(canonical = true)))
			jsonObj.mapNested("signatures.$userID.ed25519:$deviceID", signature)
		return jsonObj
	}
	
	private fun getDeviceKey() : String
	{
		return account.identityKeys()["ed25519"] ?: ""
	}
	
	private fun getIdentityKey() : String
	{
		return account.identityKeys()["curve25519"] ?: ""
	}
	
	data class KeyStore(val deviceId : String, val identityKey : String, val olmSession : OlmSession? = null)
}

fun verifySignature(signature : String, deviceKey : String, message : String) : Boolean
{
	try
	{
		utility.verifyEd25519Signature(signature, deviceKey, message)
	}
	catch (e : OlmException)
	{
		return false
	}
	return true
}

*/