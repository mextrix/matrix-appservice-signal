/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.db.SignalStorage
import de.devpi.signal.*
import msrd0.matrix.client.util.emptyMutableMap
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.whispersystems.libsignal.ecc.Curve
import org.whispersystems.libsignal.state.PreKeyRecord
import org.whispersystems.libsignal.state.SignedPreKeyRecord
import org.whispersystems.libsignal.util.KeyHelper
import org.whispersystems.libsignal.util.guava.Optional
import org.whispersystems.signalservice.api.SignalServiceAccountManager
import org.whispersystems.signalservice.api.crypto.AttachmentCipherInputStream
import org.whispersystems.signalservice.api.messages.*
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile
import org.whispersystems.signalservice.api.push.SignalServiceAddress
import org.whispersystems.signalservice.api.util.CredentialsProvider
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration
import org.whispersystems.signalservice.internal.push.SignalServiceProtos
import org.whispersystems.signalservice.internal.util.Util
import java.io.*
import java.util.concurrent.*

object SignalConnections
{
	private val logger : Logger = LoggerFactory.getLogger(SignalConnections::class.java)
	private const val MAX_KEYS : Int = 100
	private const val MAX_SIZE : Int = 200
	
	private val executor : ScheduledExecutorService = Executors.newScheduledThreadPool(2)
	private val storage : MutableMap<String, SignalAccount> = emptyMutableMap()
	
	init
	{
		executor.scheduleAtFixedRate({
			storage.values.forEach { it.messagePipe.sendKeepAlive() }
		}, 0, 50, TimeUnit.SECONDS)
		executor.scheduleAtFixedRate({
			storage.values.forEach { updateKeys(it) }
		}, 1, 10, TimeUnit.MINUTES)
		
	}
	
	fun retrieveAttachment(user : String, pointer : SignalServiceAttachmentPointer) : InputStream
	{
		logger.info("Retrieving pointer attachment")
		val account = storage[user] ?: throw IllegalArgumentException("User $user does not exist")
		val tmpFile = File.createTempFile("mas", null)
		tmpFile.deleteOnExit()
		account.socket.retrieveAttachment(pointer.relay.orNull(), pointer.id, tmpFile, Int.MAX_VALUE, null)
		//TODO use file if attachment to big
		val tOut = ByteArrayOutputStream()
		val cIn = AttachmentCipherInputStream.createFor(tmpFile, 0, pointer.key, pointer.digest.get())
		Util.copy(cIn, tOut)
		return ByteArrayInputStream(tOut.toByteArray())
	}
	
	fun retrieveProfile(user : String, address : SignalServiceAddress) : SignalServiceProfile
	{
		val account = storage[user] ?: throw IllegalArgumentException("User $user does not exist")
		return account.socket.retrieveProfile(address)
	}
	
	fun addAccount(protocolStore : SignalStorage, new : Boolean = true)
	{
		logger.info("adding account ${protocolStore.number}")
		
		val credentials = protocolStore.credentials
		val number = protocolStore.number
		val adr = SignalServiceAddress(number)
		val pipe = AsyncMessagePipe(credentials)
		val accountManager = SignalServiceAccountManager(signalServiceConfiguration,
				credentials.user, credentials.password, "OWD")
		val cipher = SignalCipher(adr, protocolStore)
		val socket = PushServiceSocket(signalServiceConfiguration, credentials, "OWD")
		val sender = SignalMessageSender(credentials.user, credentials.password, socket, protocolStore)
		val handler = SignalHandling(transaction { protocolStore.bridgeUser }, number)
		
		val consumer : (SignalServiceEnvelope) -> Unit = {
			if (storage[number] != null)
			{
				if (it.type == SignalServiceProtos.Envelope.Type.RECEIPT_VALUE)
					logger.info("Receiving read receipt")
				else
					try
					{
						val msg = cipher.decrypt(it)
						handler.onSignalMessage(msg, it.source)
					}
					catch (e : Exception)
					{
						val msg = SignalServiceContent(
								SignalServiceDataMessage(System.currentTimeMillis(), "Unable to decrypt content!"))
						handler.onSignalMessage(msg, it.source)
						logger.warn("Couldn't decrypt cipher, probably an issue with the message itself", e)
					}
			}
		}
		
		pipe.addListener(consumer)
		//accountManager.setAccountAttributes(credentials.signalingKey, registrationId, true)
		val info = SignalAccount(credentials, pipe, protocolStore, cipher, accountManager, socket, sender,
				consumer.hashCode(), handler)
		storage[number] = info
		if (new)
			updateKeys(info, true)
		pipe.connect()
	}
	
	fun removeAccount(user : String)
	{
		val account = storage[user]
		account?.messagePipe?.removeListener(account.consumerHash)
		storage.remove(user)
	}
	
	private fun updateKeys(signalAccount : SignalAccount, force : Boolean = false)
	{
		val currentlyStored = signalAccount.accountManager.preKeysCount
		if (currentlyStored <= 10 || force)
		{
			logger.info("Updating PreKeys, currently stored: $currentlyStored")
			var (cNextPreKey, cNextSignedPrekey) =
					transaction {
						signalAccount.protocolStore.nextPreKey to signalAccount.protocolStore.nextSignedPreKey
					}
			val preKeys : MutableList<PreKeyRecord> = mutableListOf()
			for (i in 0..MAX_KEYS)
			{
				val keyPair = Curve.generateKeyPair()
				val id = (cNextPreKey + i) % MAX_SIZE
				val record = PreKeyRecord(id, keyPair)
				signalAccount.protocolStore.storePreKey(id, record)
				preKeys.add(record)
			}
			cNextPreKey = (cNextPreKey + MAX_KEYS + 1) % MAX_SIZE
			val signedPreKey : SignedPreKeyRecord = KeyHelper.generateSignedPreKey(
					signalAccount.protocolStore.identityKeyPair, cNextSignedPrekey)
			signalAccount.protocolStore.storeSignedPreKey(cNextSignedPrekey, signedPreKey)
			cNextSignedPrekey = (cNextSignedPrekey + 1) % MAX_SIZE
			signalAccount.accountManager.setPreKeys(signalAccount.protocolStore.identityKeyPair.publicKey, signedPreKey,
					preKeys)
			transaction {
				signalAccount.protocolStore.nextPreKey = cNextPreKey
				signalAccount.protocolStore.nextSignedPreKey = cNextSignedPrekey
			}
		}
	}
	
	fun getDevices(user : String) : List<DeviceInfo>
	{
		val account = storage[user] ?: throw IllegalArgumentException("User $user does not exist")
		return account.socket.devices
	}
	
	fun getMessageSender(user : String) : SignalMessageSender
	{
		val account = storage[user] ?: throw IllegalArgumentException("User $user does not exist")
		return account.messageSender
	}
	
	fun getAttachment(data : Pair<ByteArray, String>)
			= getAttachment(data.second, data.first)
	
	fun getAttachment(contentType : String, bytes : ByteArray)
			= getAttachment(contentType, bytes.size.toLong(), ByteArrayInputStream(bytes))
	
	fun getAttachment(contentType : String, len : Long, stream : InputStream) : SignalServiceAttachmentStream
	{
		return SignalServiceAttachmentStream(stream, contentType, len, Optional.absent(), false, null)
	}
	
	fun sendMessage(sender : String, data : SignalServiceDataMessage, vararg recipients : SignalAddress)
			= sendMessage(sender, data, recipients.asList())
	
	fun sendMessage(sender : String, data : SignalServiceDataMessage, recipients : List<SignalAddress>)
	{
		logger.info("Sending new message as $sender")
		val msgSender = storage[sender]?.messageSender ?: throw IllegalArgumentException("User $sender does not exist")
		when
		{
			recipients.isEmpty() -> throw IllegalArgumentException("You must at least specify one recipient")
			recipients.size == 1 -> msgSender.sendMessage(recipients[0], data)
			else                 -> msgSender.sendMessage(recipients, data)
		}
	}
	
	fun sendMessage(sender : String, data : SignalServiceProtos.SyncMessage.Request)
	{
		val msgSender = storage[sender]?.messageSender ?: throw IllegalArgumentException("User $sender does not exist")
		msgSender.sendRequest(data)
	}
	
	fun shutdown()
	{
		executor.shutdown()
		for ((_, pipe, _, _, _, _) in storage.values)
			pipe.disconnect()
	}
	
	data class SignalAccount(val credentials : CredentialsProvider, val messagePipe : AsyncMessagePipe,
							 val protocolStore : SignalStorage, val cipher : SignalCipher,
							 val accountManager : SignalServiceAccountManager, val socket : PushServiceSocket,
							 val messageSender : SignalMessageSender, val consumerHash : Int,
							 val handler : SignalHandling)
	
	@JvmStatic
	fun loadFromDb()
	{
		val storages = transaction { SignalStorage.all().toList() }
		SignalConnections.logger.info("Restoring ${storages.size} accounts")
		storages.forEach {
			SignalConnections.logger.info("Restoring account ${it.number}")
			SignalConnections.addAccount(it, false)
			SignalConnections.logger.info("Finished restoring account ${it.number}")
		}
	}
}

