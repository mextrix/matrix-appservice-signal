/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import de.devpi.matrix.`as`.SignalBotClient.bot
import de.devpi.matrix.db.*
import de.devpi.matrix.db.Contacts
import de.devpi.signal.*
import msrd0.matrix.client.*
import msrd0.matrix.client.event.*
import msrd0.matrix.client.util.toImage
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.whispersystems.libsignal.SignalProtocolAddress
import org.whispersystems.libsignal.util.guava.Optional
import org.whispersystems.signalservice.api.messages.*
import org.whispersystems.signalservice.api.messages.multidevice.*
import org.whispersystems.signalservice.api.push.TrustStore
import org.whispersystems.signalservice.internal.configuration.*
import org.whispersystems.signalservice.internal.push.SignalServiceProtos.SyncMessage.*
import java.awt.image.BufferedImage
import java.awt.image.RenderedImage
import java.io.InputStream
import javax.imageio.ImageIO

/**
 * A signal trust store implementation that uses the system resource `whisper.store` with the password `whisper`.
 */
class SignalTrustStore : TrustStore
{
	override fun getKeyStorePassword() = "whisper"
	override fun getKeyStoreInputStream() : InputStream = ClassLoader.getSystemResourceAsStream("whisper.store")
}

/** The default signal trust store. */
@JvmField
val signalTrustStore = SignalTrustStore()
/** The default signal urls. */
@JvmField
val signalUrls = arrayOf(SignalServiceUrl("https://textsecure-service.whispersystems.org", signalTrustStore))
/** The default signal cdn urls. */
@JvmField
val signalCdnUrls = arrayOf(SignalCdnUrl("https://cdn.signal.org", signalTrustStore))
/** The default signal service configuration. */
@JvmField
val signalServiceConfiguration = SignalServiceConfiguration(signalUrls, signalCdnUrls )

/**
 * Generates a new signal registration
 */
fun newSignalRegistration(room : Room, sender : AdminRoomMember) : RenderedImage
{
	val registration = SignalRegistration(signalServiceConfiguration)
	registration.onScan {
		val (number, deviceId) = it.credentialsProvider.user.split(".")
		val senderDisplay : String = SignalAdminClient.adminClient.displayname(
				sender.userId) ?: sender.userId.toString()
		val senderDisplayLink = """<a href="https://matrix.to/#/${sender.userId}">$senderDisplay</a>"""
		room.sendMessage(FormattedTextMessageContent(
				"$senderDisplay: You have successfully linked your Signal account!",
				TextMessageFormats.HTML,
				"<p>$senderDisplayLink: You have successfully linked your Signal account!</p>"))
		val accountStore = transaction {
			sender.status = AdminRoomMemberStatus.REGISTERED
			val bridgeUser = BridgeUser.new {
				userId = sender.userId
				this.number = number
			}
			val contact = Contact.new {
				this.bridgeUser = bridgeUser
				matrixId = computeUserId(bridgeUser.number, bridgeUser.number)
				asUser = getUser(matrixId)
				this.number = bridgeUser.number
				this.name = bridgeUser.number
			}
			BridgeUserSelfContact.new {
				this.bridgeUser = bridgeUser
				this.contact = contact
			}
			return@transaction SignalStorage.new {
				this.bridgeUser = bridgeUser
				registrationId = it.registrationId
				identityKey = it.identity
				credentials = it.credentialsProvider
			}
		}
		accountStore.saveIdentity(SignalProtocolAddress(number, accountStore.deviceid), it.identity.publicKey)
		SignalConnections.addAccount(accountStore)
		val devices = SignalConnections.getDevices(number)
		val mismatched = MismatchedSignalDevices(
				missingSignalDevices = devices.map { it.id.toInt() }.filter { it != deviceId.toInt() })
		SignalConnections.getMessageSender(number).handleMismatchedDevices(mismatched)
		SignalConnections.sendMessage(number, Request.newBuilder().setType(Request.Type.CONTACTS).build())
		SignalConnections.sendMessage(number, Request.newBuilder().setType(Request.Type.GROUPS).build())
	}
	return registration.authenticationQRCode
}

/**
 * Handles receiving Signal messages.
 *
 * This class is used in SignalConnections
 */
class SignalHandling(private val bridgeUser : BridgeUser, private val number : String)
{
	companion object
	{
		private val logging : Logger = LoggerFactory.getLogger(SignalHandling::class.java)
	}
	
	/**
	 * Processes decrypted messages received from Signal
	 */
	fun onSignalMessage(message : SignalServiceContent, originNumber : String, destinationNumber : String = "")
	{
		logging.info("Received a new message")
		if (message.syncMessage.isPresent)
		{
			logging.info("New sync message")
			val syncMessage = message.syncMessage.get()
			if (syncMessage.sent.isPresent)
			{
				logging.info("New sync:sent transcript")
				val sentTranscript : SentTranscriptMessage = syncMessage.sent.get()
				val content = SignalServiceContent(sentTranscript.message)
				onSignalMessage(content, originNumber, sentTranscript.destination.or(""))
			}
			if (syncMessage.contacts.isPresent)
			{
				logging.info("New sync:contacts update")
				val atStream : SignalServiceAttachment = syncMessage.contacts.get().contactsStream
				val inputStream : InputStream =
						if (atStream.isPointer)
							SignalConnections.retrieveAttachment(number, atStream.asPointer())
						else
							atStream.asStream().inputStream
				val contactsStream = DeviceContactsInputStream(inputStream)
				val contactList : HashSet<DeviceContact> = hashSetOf()
				
				while (inputStream.available() > 0)
				{
					try
					{
						var contact : DeviceContact = contactsStream.read()
						if (contact.avatar.isPresent && contact.avatar.get().isStream)
						{
							val oldAttachment = contact.avatar.get()
							val newAttachment = oldAttachment.copy()
							contact = DeviceContact(contact.number, contact.name, Optional.of(newAttachment),
									contact.color, contact.verified, Optional.absent())
						}
						contactList.add(contact)
					}
					catch (exception : Exception)
					{
						logging.warn("Stream ended", exception)
						break
					}
				}
				
				logging.info("Read ${contactList.size} contact(s) successfully")
				transaction {
					contactList.forEach {
						updateDbContact(it).updateDirectChatRoom()
					}
				}
			}
			if (syncMessage.groups.isPresent)
			{
				logging.info("New sync:groups update")
				val atStream = syncMessage.groups.get()
				val inputStream : InputStream =
						if (atStream.isPointer)
							SignalConnections.retrieveAttachment(number, atStream.asPointer())
						else
							atStream.asStream().inputStream
				val groupsStream = DeviceGroupsInputStream(inputStream)
				val groupList : HashSet<DeviceGroup> = hashSetOf()
				
				while (inputStream.available() > 0)
				{
					try
					{
						var group : DeviceGroup = groupsStream.read()
						if (group.isActive)
						{
							if (group.avatar.isPresent && group.avatar.get().isStream)
							{
								val oldAttachment = group.avatar.get()
								val newAttachment = oldAttachment.copy()
								group = DeviceGroup(group.id, group.name, group.members, Optional.of(newAttachment),
										true)
							}
							groupList.add(group)
						}
					}
					catch (exception : Exception)
					{
						logging.warn("Stream ended", exception)
						break
					}
				}
				
				logging.info("Read ${groupList.size} group(s) successfully")
				transaction {
					groupList.forEach {
						updateDbGroup(it.toSignalServiceGroup())
					}
				}
			}
			if (syncMessage.blockedList.isPresent)
			{
				logging.info("New sync:blocked list")
				//TODO is it necessary to change something?
			}
			if (syncMessage.read.isPresent)
				logging.info("New sync:read messages")
			if (syncMessage.request.isPresent)
				logging.info("New sync:requests, ignoring")
			if (syncMessage.verified.isPresent)
				logging.info("New sync:verified messages, ignoring")
		}
		if (message.dataMessage.isPresent)
		{
			logging.info("New data message")
			val dataMessage = message.dataMessage.get()
			
			if (dataMessage.groupInfo.isPresent)
			{
				//group handling
				val groupInfo : SignalServiceGroup = dataMessage.groupInfo.get()
				when (groupInfo.type)
				{
					SignalServiceGroup.Type.DELIVER      ->
					{
						transaction {
							AsRoom.find {
								AsRooms.bridgeUser eq bridgeUser.id and (AsRooms.signalId eq String(groupInfo.groupId))
							}.firstOrNull()
						}?.apply { sendMessage(originNumber, dataMessage, this) }
					}
					SignalServiceGroup.Type.UPDATE       ->
					{
						updateDbGroup(groupInfo)
					}
					SignalServiceGroup.Type.QUIT         ->
					{
						//TODO: is it necessary to handle this for the bridge user himself?
						transaction {
							AsRoom.find {
								AsRooms.bridgeUser eq bridgeUser.id and (AsRooms.signalId eq String(groupInfo.groupId))
							}.forFirst {
								logging.info("Deleting user $originNumber from group ${String(groupInfo.groupId)}")
								ContactRoomAssoc.find { ContactRoomAssocs.room eq this@forFirst.id }.filter {
									it.contact.number == originNumber && it.contact.directChatRoom == null
								}.forFirst {
									this.contact.delete()
									this.delete()
								}
							}
						}
					}
					else                                 ->
						logging.info("No group update, ignoring message of type ${groupInfo.type}")
				}
			}
			else
			{
				if (dataMessage.isEndSession)
				{
					transaction {
						SignalStorage.find { SignalStorages.bridgeUser eq bridgeUser.id }.forFirst {
							if (destinationNumber == "")
								deleteAllSessions(originNumber)
							else
								deleteAllSessions(destinationNumber)
						}
					}
				}
				sendMessage(originNumber, destinationNumber, dataMessage)
			}
		}
		if (message.callMessage.isPresent)
		{
			logging.info("New call message")
			val callMessage = message.callMessage
			TODO("not implemented")
		}
	}
	
	private fun sendMessage(sender : String, dataMessage : SignalServiceDataMessage, signalRoom : AsRoom)
	{
		val contact = transaction {
			updateDbContact(sender).asUser
		}
		sendMessage(dataMessage, signalRoom, contact)
	}
	
	private fun sendMessage(dataMessage : SignalServiceDataMessage, asRoom : AsRoom, contact : AsUser)
	{
		val cli = contact.matrixClient
		val room = Room(cli, asRoom.matrixId)
		
		//TODO: send directly as stream and add voicenotes
		dataMessage.attachments.orNull()?.forEach {
			val (inputStream, name) =
					if (it.isPointer)
						SignalConnections.retrieveAttachment(number, it.asPointer()) to it.asPointer().fileName.or("")
					else
						it.asStream().inputStream to ""
			when (it.contentType)
			{
				"image/png", "image/jpeg", "image/gif" ->
				{
					val img = ImageIO.read(inputStream)
					val msg = img.upload(cli)
					room.sendMessage(msg)
				}
				else                                   ->
				{
					val msg = FileMessageContent(name)
					msg.uploadFile(inputStream.readBytes(), it.contentType ?: "application/octet-stream", cli)
					room.sendMessage(msg)
				}
			}
		}
		if (dataMessage.body.isPresent)
		{
			val txt = dataMessage.body.get()
			room.sendMessage(TextMessageContent(txt))
		}
	}
	
	private fun sendMessage(sender : String, destinationNumber : String, dataMessage : SignalServiceDataMessage)
	{
		val (room, contact) = transaction {
			val contact = updateDbContact(sender)
			val dbroom : AsRoom =
					if (destinationNumber == "")
						contact.updateDirectChatRoom(true)
					else
					{
						val dContact = updateDbContact(destinationNumber)
						dContact.updateDirectChatRoom(true)
					}
			return@transaction dbroom to contact.asUser
		}
		sendMessage(dataMessage, room, contact)
	}
	
	private fun createMissingContacts(contacts : List<String>, room : AsRoom)
	{
		contacts.forEach {
			val dContact = updateDbContact(it)
			contactToRoom(dContact, room)
		}
	}
	
	private fun contactToRoom(contact : Contact, asroom : AsRoom, invite : Boolean = true)
	{
		ContactRoomAssoc.find { ContactRoomAssocs.contact eq contact.id and (ContactRoomAssocs.room eq asroom.id) }.getOrNew {
			this.contact = contact
			this.room = asroom
			if (invite)
				asroom.invite(contact)
		}
	}
	
	private fun Contact.updateDirectChatRoom(noUpdate : Boolean = false) : AsRoom
	{
		val contact = this
		val room = contact.directChatRoom.updateOrNew(noUpdate) {
			if (it)
			{
				this@updateOrNew.bridgeUser = this@SignalHandling.bridgeUser
				this@updateOrNew.asUser = contact.asUser
			}
			this@updateOrNew.name = contact.name
			if (contact.avatar != null)
				this@updateOrNew.avatar = contact.avatar
		}
		logging.info("Creating new directChatRoom for ${contact.name} with id ${room.matrixId}")
		contact.directChatRoom = room
		contactToRoom(contact, room, false)
		contactToRoom(bridgeUser.selfSignalContact, room)
		return room
	}
	
	private fun updateDbContact(contact : DeviceContact, noUpdate : Boolean = false) : Contact
	{
		return Contact.find {
			Contacts.bridgeUser eq bridgeUser.id and (Contacts.number eq contact.number)
		}.updateOrNew(noUpdate) {
			if (it)
			{
				bridgeUser = this@SignalHandling.bridgeUser
				matrixId = computeUserId(this@SignalHandling.bridgeUser.number, contact.number)
				asUser = getUser(matrixId)
				number = contact.number
			}
			name = if (contact.name.isPresent)
				contact.name.get()
			else
				contact.number
			if (contact.avatar.isPresent)
			{
				val avatarAttachment = contact.avatar.get()
				avatar = avatarAttachment.asImage()
			}
		}
	}
	
	private fun updateDbContact(contactNumber : String) : Contact
	{
		val devContact = DeviceContact(contactNumber, Optional.absent(), Optional.absent(), Optional.of(""),
				Optional.absent(), Optional.absent())
		return updateDbContact(devContact, true)
	}
	
	private fun updateDbGroup(groupInfo : SignalServiceGroup) : AsRoom
	{
		val groupId = String(groupInfo.groupId)
		return transaction {
			val room = AsRoom.find {
				AsRooms.bridgeUser eq bridgeUser.id and (AsRooms.signalId eq groupId)
			}.updateOrNew {
				if (it)
				{
					this.bridgeUser = this@SignalHandling.bridgeUser
					this.asUser = bot
					signalId = groupId
				}
				if (groupInfo.name.isPresent)
					this.name = groupInfo.name.get()
				else
					this.name = groupId
				if (groupInfo.avatar.isPresent)
					this.avatar = groupInfo.avatar.get().asImage()
			}
			
			if (groupInfo.members.isPresent)
				createMissingContacts(groupInfo.members.get(), room)
			return@transaction room
		}
	}
	
	private fun SignalServiceAttachment.asImage() : RenderedImage
	{
		return if (this.isStream)
			ImageIO.read(this.asStream().inputStream)
		else
			ImageIO.read(SignalConnections.retrieveAttachment(number, this.asPointer()))
	}
}

fun RenderedImage.upload(client : MatrixClient) : ImageMessageContent
{
	try
	{
		val messageContent = ImageMessageContent("")
		messageContent.uploadImage(this, client)
		return messageContent
	}
	catch (ex : MatrixAnswerException)
	{
		// if the image was too large, try scaling the image down and re-upload
		if (ex is MatrixErrorResponseException && ex.errcode == "413" && width > 96 && height > 96)
		{
			val img = BufferedImage(width / 2, height / 2,
					if (colorModel.hasAlpha()) BufferedImage.TYPE_INT_ARGB else BufferedImage.TYPE_INT_RGB)
			val g = img.createGraphics()
			g.drawImage(toImage(), 0, 0, img.width, img.height, null)
			return img.upload(client)
		}
		
		error("Not able to upload image")
	}
}