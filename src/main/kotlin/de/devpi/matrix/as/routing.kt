/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import com.beust.klaxon.*
import de.devpi.matrix.Configuration.hsConfig
import de.devpi.matrix.currClassName
import de.devpi.matrix.db.*
import msrd0.matrix.client.*
import org.eclipse.jetty.util.Jetty
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.slf4j.*
import spark.*
import spark.Spark.*
import java.io.StringReader
import java.security.SecureRandom
import java.util.*

private val logger : Logger = LoggerFactory.getLogger(currClassName())
val rng = SecureRandom()

/** The value that will be used in the `Server` or `User-Agent` header. */
val applicationIdentification = "Matrix AppService Signal (SparkJava Jetty/${Jetty.VERSION})"

fun ByteArray.base64() : String
		= Base64.getEncoder().encodeToString(this)

val Request.isAuthenticated : Boolean
	get() = queryParams("access_token") == hsConfig.token

val Request.json : JsonObject
	get() = Parser().parse(StringReader(body())) as JsonObject

fun Response.typeJson(charset : String = "utf-8") = type("application/json;charset=$charset")

class UnauthenticatedException : RuntimeException()
class NotFoundException(val msg : String) : RuntimeException()

@JvmOverloads
fun routing(serverPort : Int = 8181)
{
	port(serverPort)
	
	before("*", { req, res ->
		logger.info("${req.requestMethod().toUpperCase()} ${req.url()} from ${req.ip()} (${req.userAgent()})")
		res.header("Server", applicationIdentification)
	})
	
	put("/transactions/:txnId", { req, res ->
		if (!req.isAuthenticated)
			throw UnauthenticatedException()
		val txnId = req.params("txnId").toLong()
		
		var dup = false
		transaction {
			if (AsTransaction.find { AsTransactions.txnId eq txnId }.empty())
				AsTransaction.new {
					this.txnId = txnId
					this.received = DateTime.now()
					this.json = req.body()
				}
			else
				dup = true
		}
		
		if (dup)
		{
			logger.info("Received duplicate transaction $txnId")
		}
		else
		{
			val events = req.json.array<JsonObject>("events") ?: missing("events")
			for (event in events)
			{
				logger.info(event.toJsonString())
				handleEvent(event)
			}
		}
		
		res.typeJson()
		"{}"
	})
	
	get("/rooms/:roomAlias", { req, res ->
		if (!req.isAuthenticated)
			throw UnauthenticatedException()
		
		val roomAlias = req.params("roomAlias")!!
		logger.info("hs requested room $roomAlias")
		
		throw NotFoundException("Not Implemented")
	})
	
	get("/users/:userId", { req, res ->
		if (!req.isAuthenticated)
			throw UnauthenticatedException()
		
		val userId = req.params("userId")!!
		logger.info("hs requested user $userId")
		
		if (transaction { AsUser.find { AsUsers.userId eq userId }.firstOrNull() } != null)
		{
			res.type("json")
			return@get "{}"
		}
		
		throw NotFoundException("User was not created by this AS")
	})
	
	exception(UnauthenticatedException::class.java, { _, _, res ->
		res.status(403)
		res.typeJson()
		res.body(JsonObject(mapOf(
				"errcode" to "M_UNKNOWN_TOKEN",
				"error" to "No or invalid access token supplied"
		)).toJsonString())
	})
	
	exception(NotFoundException::class.java, { ex, _, res ->
		ex as NotFoundException
		res.status(404)
		res.typeJson()
		res.body(JsonObject(mapOf(
				"errcode" to "M_NOT_FOUND",
				"error" to ex.msg
		)).toJsonString())
	})
	
	notFound({ req, res ->
		res.status(404)
		res.typeJson()
		return@notFound JsonObject(mapOf(
				"errcode" to "M_UNKNOWN",
				"error" to "404 NOT FOUND ${req.url()}"
		)).toJsonString()
	})
	
	exception(IllegalJsonException::class.java, { ex, _, res ->
		res.status(400)
		res.typeJson()
		res.body(JsonObject(mapOf(
				"errcode" to "M_BAD_JSON",
				"error" to ex.message
		)).toJsonString())
	})
	
	afterAfter({ req, res ->
		val len = res.body()?.length ?: 0
		logger.info("${req.requestMethod().toUpperCase()} ${req.url()} from ${req.ip()} - ${res.status()} $len")
		res.header("Content-Length", "$len")
	})
}
