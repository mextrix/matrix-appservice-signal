/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix.`as`

import com.beust.klaxon.*
import de.devpi.matrix.Configuration.adminUserConfig
import de.devpi.matrix.Configuration.hsConfig
import de.devpi.matrix.`as`.SignalAdminClient.adminClient
import de.devpi.matrix.`as`.SignalAdminClient.onRoomInvitation
import de.devpi.matrix.`as`.SignalAdminClient.onRoomJoin
import de.devpi.matrix.`as`.SignalAdminClient.onRoomMessage
import de.devpi.matrix.currClassName
import de.devpi.matrix.db.*
import de.devpi.signal.*
import msrd0.matrix.client.*
import msrd0.matrix.client.event.*
import msrd0.matrix.client.event.MessageTypes.*
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.*
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage

private val logger : Logger = LoggerFactory.getLogger(currClassName())

@Throws(IllegalJsonException::class)
fun handleEvent(json : JsonObject)
{
	val type = json.string("type") ?: missing("type")
	val roomId = RoomId.fromString(json.string("room_id") ?: missing("room_id"))
	val sender = MatrixId.fromString(json.string("sender") ?: missing("sender"))
	logger.debug("Received message of type $type in room $roomId")
	
	if (type == MatrixEventTypes.ROOM_MEMBER)
	{
		val membership = json.string("membership") ?: json.obj("content")?.string("membership") ?: missing("membership")
		val receiver = json.string("state_key") ?: missing("state_key")
		if (receiver == "@${adminUserConfig.localpart}:${hsConfig.domain}")
		{
			if (transaction { AsRoom.find { AsRooms.matrixId eq "$roomId" }.firstOrNull() } != null)
			{
				if (membership == "invite")
					RoomInvitation(adminClient, roomId).accept()
			}
			else if (membership == "invite")
				onRoomInvitation(RoomInvitation(adminClient, roomId))
			else if (membership == "join")
				onRoomJoin(Room(adminClient, roomId))
			return
		}
		else if (membership == "invite")
		{
			transaction {
				AsUser.find { AsUsers.userId eq "$sender" }.forFirst {
					AsUser.find { AsUsers.userId eq receiver }.forFirst {
						RoomInvitation(this.matrixClient, roomId).accept()
					}
				}
			}
		}
	}
	else if (type == MatrixEventTypes.ROOM_MESSAGE)
	{
		val msg = MessageContent.fromJson(json.obj("content") ?: missing("content"))
		
		// if the room is one of the admin rooms, post the message to the admin
		if (!transaction { AdminRoom.find { AdminRooms.roomId eq "$roomId" }.empty() })
		{
			onRoomMessage(roomId, sender, msg)
			return
		}
		
		// otherwise redirect the message to signal if the bridge user is sending
		transaction { BridgeUser.find { BridgeUsers.userId eq "$sender" }.firstOrNull() }?.apply {
			val asRoom = transaction { AsRoom.find { AsRooms.matrixId eq "$roomId" }.firstOrNull() }
			if (asRoom == null)
			{
				logger.warn("Received message from an unknown room")
				return
			}
			val signalSender = number
			val timestamp = json.long("origin_server_ts") ?: System.currentTimeMillis()
			val signalMsg = when (msg.msgtype)
			{
				TEXT  ->
				{
					if (msg.body == "TERMINATE")
						SignalServiceDataMessage.newBuilder().asEndSessionMessage().withBody(
								"TERMINATE").withTimestamp(timestamp).withOptionalGroup(asRoom).build()
					else
						SignalServiceDataMessage.newBuilder().withBody(msg.body).withTimestamp(
								timestamp).withOptionalGroup(asRoom).build()
				}
				IMAGE ->
				{
					val attachm = (msg as ImageMessageContent).url?.toSignalAttachment()
					if (attachm != null)
						SignalServiceDataMessage.newBuilder().withTimestamp(timestamp).withBody(
								msg.body).withAttachment(attachm).withOptionalGroup(asRoom).build()
					else
						SignalServiceDataMessage.newBuilder().withTimestamp(timestamp).withBody(
								msg.body).withOptionalGroup(asRoom).build()
				}
				else  ->
				{
					logger.warn("Unknown message type ${msg.msgtype}")
					return
				}
			}
			if (transaction { asRoom.directChatContact.firstOrNull()?.number == signalSender })
				SignalConnections.sendMessage(signalSender, signalMsg, SignalAddress(signalSender))
			else
				SignalConnections.sendMessage(signalSender, signalMsg, transaction {
					ContactRoomAssoc.find { ContactRoomAssocs.room eq asRoom.id and (ContactRoomAssocs.contact neq selfSignalContact.id) }
							.map { SignalAddress(it.contact.number) }
				})
		}
	}
}