/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal

import com.google.protobuf.InvalidProtocolBufferException
import org.whispersystems.libsignal.InvalidMessageException
import org.whispersystems.libsignal.state.SignalProtocolStore
import org.whispersystems.libsignal.util.guava.Optional
import org.whispersystems.signalservice.api.crypto.SignalServiceCipher
import org.whispersystems.signalservice.api.messages.*
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage
import org.whispersystems.signalservice.api.messages.multidevice.*
import org.whispersystems.signalservice.api.push.SignalServiceAddress
import org.whispersystems.signalservice.internal.push.SignalServiceProtos
import java.lang.reflect.Method

class SignalCipher(val localAddress : SignalServiceAddress, protocolStore : SignalProtocolStore) : SignalServiceCipher(
		localAddress, protocolStore)
{
	private val superC = SignalServiceCipher::class.java
	private val decryptF : Method by lazy {
		val m = superC.getDeclaredMethod("decrypt", SignalServiceEnvelope::class.java, ByteArray::class.java)
		m.isAccessible = true
		m
	}
	
	private val createSignalServiceMessageF : Method by lazy {
		val m = superC.getDeclaredMethod("createSignalServiceMessage", SignalServiceEnvelope::class.java,
				SignalServiceProtos.DataMessage::class.java)
		m.isAccessible = true
		m
	}
	
	private val createSynchronizeMessageF : Method by lazy {
		val m = superC.getDeclaredMethod("createSynchronizeMessage", SignalServiceEnvelope::class.java,
				SignalServiceProtos.SyncMessage::class.java)
		m.isAccessible = true
		m
	}
	
	private val createCallMessageF : Method by lazy {
		val m = superC.getDeclaredMethod("createCallMessage", SignalServiceProtos.CallMessage::class.java)
		m.isAccessible = true
		m
	}
	
	override fun decrypt(envelope : SignalServiceEnvelope) : SignalServiceContent
	{
		val decryptedContent : ByteArray =
				if (envelope.hasLegacyMessage())
					decryptF(this, envelope, envelope.legacyMessage) as ByteArray
				else
					decryptF(this, envelope, envelope.content) as ByteArray
		val superContent = originalDecrypt(envelope, decryptedContent)
		if (superContent.syncMessage.isPresent)
		{
			val originalSyncMessage : SignalServiceSyncMessage = superContent.syncMessage.get()
			val contentP = SignalServiceProtos.Content.parseFrom(decryptedContent)
			val content = contentP.syncMessage
			val newSyncMessage : SignalServiceSyncMessage =
					when
					{
						content.hasGroups()   ->
						{
							val blob = content.groups.blob
							val groups = SignalServiceAttachmentPointer(blob.id, blob.contentType,
									blob.key.toByteArray(), envelope.relay, Optional.of(blob.size),
									Optional.absent<ByteArray>(), Optional.of(blob.digest.toByteArray()),
									Optional.absent<String>(), false)
							SignalServiceSyncMessage.forGroups(groups)
						}
						content.hasContacts() ->
						{
							val blob = content.contacts.blob
							val contacts = ContactsMessage(
									SignalServiceAttachmentPointer(blob.id, blob.contentType, blob.key.toByteArray(),
											envelope.relay, Optional.of(blob.size), Optional.absent<ByteArray>(),
											Optional.of(blob.digest.toByteArray()), Optional.absent<String>(), false),
									content.contacts.complete)
							SignalServiceSyncMessage.forContacts(contacts)
						}
						content.hasBlocked()  ->
						{
							val blockedNumbers = content.blocked.numbersList
							val blocked = BlockedListMessage(blockedNumbers)
							SignalServiceSyncMessage.forBlocked(blocked)
						}
						else                  ->
						{
							originalSyncMessage
						}
					}
			return SignalServiceContent(newSyncMessage)
		}
		return superContent
	}
	
	private fun originalDecrypt(envelope : SignalServiceEnvelope, decrypted : ByteArray) : SignalServiceContent
	{
		try
		{
			var content = SignalServiceContent()
			
			if (envelope.hasLegacyMessage())
			{
				val message = SignalServiceProtos.DataMessage.parseFrom(decrypted)
				content = SignalServiceContent(
						createSignalServiceMessageF(this, envelope, message) as SignalServiceDataMessage)
			}
			else if (envelope.hasContent())
			{
				val message = SignalServiceProtos.Content.parseFrom(decrypted)
				
				if (message.hasDataMessage())
				{
					content = SignalServiceContent(createSignalServiceMessageF(this, envelope,
							message.dataMessage) as SignalServiceDataMessage)
				}
				else if (message.hasSyncMessage() && localAddress.number == envelope.source)
				{
					content = SignalServiceContent(
							createSynchronizeMessageF(this, envelope, message.syncMessage) as SignalServiceSyncMessage)
				}
				else if (message.hasCallMessage())
				{
					content = SignalServiceContent(
							createCallMessageF(this, message.callMessage) as SignalServiceCallMessage)
				}
			}
			
			return content
		}
		catch (e : InvalidProtocolBufferException)
		{
			throw InvalidMessageException(e)
		}
		
	}
}