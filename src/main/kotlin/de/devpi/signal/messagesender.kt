/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal

import com.google.protobuf.ByteString
import de.devpi.matrix.`as`.signalServiceConfiguration
import de.devpi.matrix.`as`.signalUrls
import org.whispersystems.libsignal.state.SignalProtocolStore
import org.whispersystems.libsignal.util.guava.Optional
import org.whispersystems.signalservice.api.SignalServiceMessageSender
import org.whispersystems.signalservice.api.push.SignalServiceAddress
import org.whispersystems.signalservice.internal.push.*
import org.whispersystems.signalservice.internal.push.PushServiceSocket
import org.whispersystems.signalservice.internal.util.Util
import java.lang.reflect.Method
import java.security.SecureRandom

class SignalMessageSender(user : String, password : String, socket : de.devpi.signal.PushServiceSocket,
						  store : SignalProtocolStore)
	: SignalServiceMessageSender(signalServiceConfiguration, user, password, store, "OWD",
		Optional.absent(), Optional.absent())
{
	private val clazz : Class<SignalServiceMessageSender> = SignalServiceMessageSender::class.java
	private val address : SignalServiceAddress
	
	init
	{
		val number = user.substringBefore(".")
		address = SignalAddress(number)
		val field = clazz.getDeclaredField("localAddress")
		field.isAccessible = true
		field.set(this, address)
		val socketF = clazz.getDeclaredField("socket")
		socketF.isAccessible = true
		socketF.set(this, socket)
	}
	
	private val sendMessageF : Method by lazy {
		val it = clazz.getDeclaredMethod("sendMessage", SignalServiceAddress::class.java, Long::class.java,
				ByteArray::class.java, Boolean::class.java)
		it.isAccessible = true
		return@lazy it
	}
	
	private val mismatchedF : Method by lazy {
		val it = clazz.getDeclaredMethod("handleMismatchedDevices", PushServiceSocket::class.java,
				SignalServiceAddress::class.java, MismatchedDevices::class.java)
		it.isAccessible = true
		return@lazy it
	}
	
	private val socket : PushServiceSocket by lazy {
		val it = clazz.getDeclaredField("socket")
		it.isAccessible = true
		return@lazy it.get(this@SignalMessageSender) as PushServiceSocket
	}
	
	fun sendRequest(message : SignalServiceProtos.SyncMessage.Request)
	{
		val random = SecureRandom()
		val padding = Util.getRandomLengthBytes(512)
		random.nextBytes(padding)
		
		val syncMessage = SignalServiceProtos.SyncMessage.newBuilder()
		syncMessage.padding = ByteString.copyFrom(padding)
		val container = SignalServiceProtos.Content.newBuilder()
		syncMessage.request = message
		val bytes = container.setSyncMessage(syncMessage).build().toByteArray()
		sendMessageF(this, address, System.currentTimeMillis(), bytes, false)
	}
	
	fun handleMismatchedDevices(mismatchedDevices : MismatchedDevices, recipient : SignalServiceAddress = address)
	{
		mismatchedF(this, socket, recipient, mismatchedDevices)
	}
}