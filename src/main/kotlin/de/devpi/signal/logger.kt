/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal

import org.slf4j.*
import org.whispersystems.libsignal.logging.SignalProtocolLogger
import org.whispersystems.libsignal.logging.SignalProtocolLogger.*

object SignalLogger : SignalProtocolLogger
{
	val logger : Logger = LoggerFactory.getLogger("org.whispersystems.libsignal")
	
	override fun log(priority : Int, tag : String?, message : String?)
			= log(priority, "[$tag] $message")
	
	fun log(priority : Int, message : String?) = when(priority) {
		VERBOSE, DEBUG -> logger.debug(message)
		INFO -> logger.info(message)
		WARN -> logger.warn(message)
		ERROR, ASSERT -> logger.error(message)
		else -> throw IllegalArgumentException("Unknown priority: $priority")
	}
}
