/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JSON Message to be send to the Signal server containing all information about this device.
 *
 * This is only needed if you wish to link your device to an already existing account. Keep in mind
 * that you have to authorize yourself with a random Base64 encoded password and your phone number
 * which is transmitted via the ProvisioningEnvelope.
 */
public class ConfirmCodeMessage {

  @JsonProperty
  private String signalingKey;

  @JsonProperty
  private boolean supportsSms;

  @JsonProperty
  private boolean fetchesMessages;

  @JsonProperty
  private int registrationId;

  @JsonProperty
  private String name;

  public ConfirmCodeMessage(String signalingKey, boolean supportsSms, boolean fetchesMessages,
      int registrationId,
      String name) {
    this.signalingKey = signalingKey;
    this.supportsSms = supportsSms;
    this.fetchesMessages = fetchesMessages;
    this.registrationId = registrationId;
    this.name = name;
  }

}
