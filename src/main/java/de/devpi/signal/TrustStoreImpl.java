/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.signal;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.whispersystems.signalservice.api.push.TrustStore;

/**
 * Created by julius on 10.06.17.
 */
public class TrustStoreImpl implements TrustStore {

  protected Path keyStore;
  protected String password = "";

  public TrustStoreImpl(Path keyStore, String pw) {
    this.keyStore = keyStore;
    password = pw;
  }

  @Override
  public InputStream getKeyStoreInputStream() {
    try {
      return Files.newInputStream(keyStore, StandardOpenOption.READ);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("Unable to read KeyStore", e);
    }
  }

  @Override
  public String getKeyStorePassword() {
    return password;
  }
}
