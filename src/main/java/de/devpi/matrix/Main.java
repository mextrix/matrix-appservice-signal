/*
 * matrix-appservice-signal
 * Copyright (C) 2017 Julius Lehmann
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package de.devpi.matrix;

import static de.devpi.matrix.Configuration.*;
import static de.devpi.matrix.as.RoutingKt.*;
import static de.devpi.matrix.as.SignalConnections.*;
import static de.devpi.matrix.db.DbKt.*;

import de.devpi.matrix.as.*;
import de.devpi.signal.SignalLogger;
import msrd0.matrix.client.MatrixAnswerException;

import org.whispersystems.libsignal.logging.SignalProtocolLoggerProvider;

public class Main
{
	public static void main(String args[])
			throws MatrixAnswerException, ClassNotFoundException
	{
		// fix the jvm if necessary
		DirtyHacks.removeCryptographyRestrictions();
		
		if (args.length >= 1)
			loadConfig(args[0]);
		else
			loadConfig();
		initDb();
		
		// we need to initialize the admin and bot client
		Class.forName(SignalAdminClient.class.getName());
		Class.forName(SignalBotClient.class.getName());
		
		// lets enable logging for the signal library
		SignalProtocolLoggerProvider.setProvider(SignalLogger.INSTANCE);
		
		loadFromDb();
		
		routing();
	}
}
